// 开始按钮
let start = document.getElementById('start');
// 头部位置
let head = document.getElementById('head');
// 小鸟位置
let bird = document.getElementById('bird');
// 管道位置
let ductWrap = document.getElementById('duct-wrap');
// 分数
let scoring = document.getElementById('scoring');
// 创建管道定时器
let createPipelineTimer = null;
// 小鸟是否死亡
let isDie = false;

// 重置
function reset() {
    isDie = false;
    // 清空分数
    scoring.innerHTML = '0';
    // 清空管道
    ductWrap.innerHTML = '';
    // 重置位置
    bird.style.top = '48px';
    // 移出死亡状态
    bird.classList.remove('die');
}
// 判断是否撞毁
function crashFn(bird, duct) {
    // 获取小鸟相关数据
    let birdLeft = bird.offsetLeft;
    let birdRight = birdLeft + bird.offsetWidth;
    let birdTop = bird.offsetTop;
    let birdBottom = birdTop + bird.offsetHeight;
    // 获取柱子相关数据
    let ductLeft = duct.parentNode.offsetLeft;
    let ductRight = ductLeft + duct.offsetWidth;
    let ductTop = duct.offsetTop;
    let ductBottom = ductTop + duct.offsetHeight;

    // 1: 小鸟的右边 >= 柱子的左边
    // 2: 小鸟的下边 >= 柱子的上边
    // 3: 小鸟的上边 <= 柱子的下边
    if (birdRight >= ductLeft && birdBottom >= ductTop && birdTop <= ductBottom) {
        return true;
    } else {
        return false;
    }
}
function collisionDetection() {
    // 获取管道
    let ductRow = document.querySelectorAll('.duct-rowr');
    for (let i = 0; i < ductRow.length; i++) {
        let isCrsh = crashFn(bird, ductRow[i]);
        if (isCrsh) {
            // 清除小鸟移动
            clearInterval(bird.timer);
            // 清除管道创建定时器
            clearInterval(createPipelineTimer);
            // 已死亡
            isDie = true;
            bird.classList.add('die');
        }
    }
}
// 小鸟移动
function birdMove() {
    // 速度
    bird.speed = 0;
    bird.timer = setInterval(() => {
        bird.speed += 0.5;
        bird.style.top = bird.offsetTop + bird.speed + 'px';
        // 碰到地面，停止掉落
        if (bird.offsetTop > 395) {
            bird.style.top = '395px';
            // 清除小鸟移动
            clearInterval(bird.timer);
            // 清除管道创建定时器
            clearInterval(createPipelineTimer);
            isDie = true;
        }
        // 碰到天空，不可继续往上
        if (bird.offsetTop <= 0) {
            bird.style.top = '0px';
        }

        if (bird.speed < 0) {
            bird.children[0].classList.add('bird-img-up');
        } else {
            bird.children[0].classList.remove('bird-img-up');
        }
        // 碰撞检测
        collisionDetection();
        if (isDie) {
            this.parentNode.style.display = 'block'
        }
    }, 30);
}

// 范围内随机值min~max之间的数
function rangeRandom(min, max) {
    let gap = max - min;
    const num = Math.random() * gap;
    return num + min;
}

// 管道动起来
function pipelineMove(duct, time = 30) {
    duct.l = 350;
    // 解决一次加很多分数（因为规定时间内的数量太多）
    duct.scoreBool = true;
    duct.moveTimer = setInterval(() => {
        // 左移动
        duct.l -= 3;
        duct.style.left = duct.l + 'px';

        // 如果超出，移出
        if (duct.l < -62) {
            ductWrap.removeChild(duct);
        }

        //  过一个管道超出一半，分数增加
        if (duct.l < -31) {
            duct.scoreBool && (scoring.innerHTML = +scoring.innerHTML + 10);
            duct.scoreBool = false;
        }

        isDie && clearInterval(duct.moveTimer);
    }, time);
}

// 创建管道, 核心：定时器
function createPipeline() {
    createPipelineTimer = setInterval(() => {
        let duct = document.createElement('li');
        // 上下管道高度 61~261之间
        let upHeight = rangeRandom(61, 261);
        // 100是可通过间距
        let downHeight = 424 - upHeight - 100;
        duct.innerHTML = `
            <div class="duct-up duct-rowr" style="height: ${upHeight}px">
                <img src="./images/pipeDown.png" />
            </div>
            <div class="duct-down duct-rowr" style="height: ${downHeight}px">
                <img src="./images/pipeUp.png" />
            </div>
        `;
        ductWrap.append(duct);
        pipelineMove(duct);
    }, 3000);
}

function startClick() {
    reset();
    head.style.display = 'none';
    this.parentNode.style.display = 'none';
    bird.style.display = 'block';
    // 小鸟移动
    birdMove.call(this);
    // 鼠标点击
    document.addEventListener('mousedown', () => {
        // 给一个对抗
        bird.speed = -5;
    });
    createPipeline();
}

function init() {
    start.addEventListener('click', startClick);
}

init();
